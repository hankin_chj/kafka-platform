package com.chj.sendtype;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 1.lambda表达式
 * 2.stream编程
 * 3.方法与构造函数引用，允许你使用 :: 关键字来传递方法或者构造函数引用(非静态方法)
 * 4.optional解决空指针异常
 * 5.
 */
public class Stream {
    private static List<String> list = Arrays.asList("a", "b", "c","e","d");

        public static void main(String[] args) {
//            System.out.println(Java8.staticMethod());
//            new Java8Impl(System.out::println);
//            //原始集合排序和lambda排序
//            list.sort(new Comparator<String>() {
//                @Override
//                public int compare(String o1, String o2) {
//                    return o1.compareTo(o2);
//                }
//            });

            list.sort((o1, o2) -> o1.compareTo(o2));
            //与上面代码功能一样  类(或实例对象) :: 方法
            list.sort(String::compareTo);

            //流式编程
            stream();

            //解决空指针
            optional();
        }

        private static void stream() {
            System.out.println("遍历集合");
            Consumer<String> consumer = System.out::println;
            list.forEach(consumer);

            System.out.println("过滤字符串是a和b");
            list.stream().filter((p) -> "a".equals(p))
                    .filter((p) -> "b".equals(p))
                    .forEach(consumer);

            System.out.println("选出前2个");
            list.stream()
                    .limit(2)
                    .forEach(consumer);


            list = Arrays.asList("a", "b", "c", "e", "d");
            System.out.println("排序");
            List<String> sortList = list.stream()
                    .sorted((s1, s2) -> s1.compareTo(s2))
                    .collect(Collectors.toList());
            sortList.forEach(System.out::println);

            System.out.println("排序后的最高一个和最低一个（在数值上比较好理解）");
            String max=list.stream()
                    .max(String::compareTo)
                    .get();
            System.out.println(max);
            String min=list.stream()
                    .min(String::compareTo)
                    .get();
            System.out.println(min);

            System.out.println("结合map(),可以使用collect()将结果放到字符串，set或treeSet中");
            String collectStr = list.stream()
                    .map(s -> s + "1")
                    .collect(Collectors.joining(";"));
            System.out.println(collectStr);
            Set<String> set=list.stream().map(s->s+"1").collect(Collectors.toSet());
            TreeSet<String> treeSet=list.stream().map(s->s+"1").collect(Collectors.toCollection(TreeSet::new));

            System.out.println("并行(parallelStream)计算总和(sum)");
            List<Integer> intList=Arrays.asList(1,2,3);
            int sum = intList.parallelStream()
                    .mapToInt(i->i)
                    .sum();
            System.out.println(sum);

            System.out.println("使用summaryStatistics方法获得stream 中元素的各种汇总数据");
            IntSummaryStatistics intSummaryStatistics = intList.stream()
                    .mapToInt(i -> i)
                    .summaryStatistics();
            System.out.println("平均值="+intSummaryStatistics.getAverage());
            System.out.println("大小="+intSummaryStatistics.getCount());
            System.out.println("最大值="+intSummaryStatistics.getMax());
        }

        private static void optional() {
            String str=null;
            //如果Optional实例持有一个非空值，则isPresent()方法返回true，否则返回false；
            Optional<String> optionalS=Optional.ofNullable(str);
            boolean isNotNull = optionalS.isPresent();
            if (isNotNull){
                System.out.println(optionalS.get());
            }
            //在持有null的时候返回传入的默认值。orElse()和orElseGet()方法类似
            System.out.println(optionalS.orElse("defaulValue"));
            System.out.println(optionalS.orElseGet(()->"defaultValue"));
        }

}
