package com.chj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTrafficShapingApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTrafficShapingApplication.class, args);
    }

}
