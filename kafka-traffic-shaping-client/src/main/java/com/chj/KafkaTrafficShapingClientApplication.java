package com.chj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTrafficShapingClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaTrafficShapingClientApplication.class, args);
    }

}
